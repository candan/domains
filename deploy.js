#!/usr/bin/env node

var accounts = require("./accounts.json");
var domains = require("./domains.json");
var util = require("util");
var process = require("process");
var spawnSync = require('child_process').spawnSync;
var slug = require('slug');

/* convert env name */
function getEnvName(account, provider, name) {
  var _account = slug(account, '_').toUpperCase();
  var _provider = slug(provider, '_').toUpperCase();
  var res = 'ACCOUNT_' + _account + '_' + _provider + '_' + name;
  return res.toUpperCase();
}

console.log("Running...");

Object.keys(domains).forEach(function (dn) {
  var domain = domains[dn];
  var provider = accounts[domain.account].provider;

  if (!process.env[getEnvName(domain.account, provider, 'id')]) {
    console.error(getEnvName(domain.account, provider, 'id') + ' is missing.');
    process.exit();
  }
  if (!process.env[getEnvName(domain.account, provider, 'password')]) {
    console.error(getEnvName(domain.account, provider, 'password') + ' is missing.');
    process.exit();
  }

  var args = [
    './providers/' + provider + '.js',
    '--domainname=' + dn,
    '--domainid=' + domain.domainid,
    '--zonefile=' + domain.zonefile,
    '--id=' + process.env[getEnvName(domain.account, provider, 'id')],
    '--password=' + process.env[getEnvName(domain.account, provider, 'password')]
  ];

  spawnSync('casperjs', args, {stdio: 'inherit'});
});

console.log("Finished.");
